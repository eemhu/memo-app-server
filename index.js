const express = require('express');
const cors = require('cors');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
var store = new MongoDBStore({
    uri: 'mongodb://localhost:27017/memo-app',
    collection: 'sessions'
});

store.on('error', err => {
    console.log(err);
});
// const methodOverride = ... ; (not installed?)
// const cookieParser = ... ; Not needed!
// app.use session({})

const app = express();
const port = 3001;

var DatabaseFunctions = require('./database.js');
var dbf = new DatabaseFunctions();

const mongoClient = require('mongodb').MongoClient;
const mongoUrl = "mongodb://localhost:27017/";

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(session({
    secret : 'dummy secret',
    resave : false,
    saveUninitialized : true,
    cookie : { maxAge: 1000 * 60 * 60 * 24, secure : false },
    store : store
}));
app.use('/', express.static('../memo-app/build'));


app.listen(port, () => {
    console.log(`memo-app-server listening at ${port}`);
});

app.get('/test-session', (req, res) => {
    res.send(JSON.stringify(req.session));
});

mongoClient.connect(mongoUrl, { useUnifiedTopology : true }, (err, db) => {
    if (err) throw err;
    var dbo = db.db("memo-app");

    app.get('/authchk', (req, res) => {
        if (req.session.isAuthorized && req.session.username) {
            res.json({ auth: true, username: req.session.username});
        }
        else {
            res.json({ auth: false, username: null });
        }
    });

    app.post('/login', (req, res) => {
        // TODO: implement login w/ existing credentials on mongoDB
        // Add hashing later

        let username = req.body.username;
        let password = req.body.password;
        console.log(`Username: ${username} Password: ${password}`);

        dbf.getUsers(dbo, username, password, (data => {
            //console.log(data.Length);
            //console.log(data[0].username);
            //console.log(data[0].password);
            console.table(data);
            console.log("Table size = " + data.length);
            if (data && data.length === 1) {
                // good
                req.session.isAuthorized = true;
                req.session.username = username;
                
                res.json({
                    auth : true,
                    username : username
                });
            }
            else {
                res.json({
                    auth : false,
                    username : username
                });
            }
        }));
    });

    app.get('/logout', (req, res) => {
        if (req.session.isAuthorized && req.session.username) {
            req.session.destroy();
            res.json({
                isLoggedOut: true,
                msg : "wasLoggedInBefore"
            });
        }
        else {
            res.json({
                isLoggedOut: true,
                msg : "notLoggedInBefore"
            });
        }
    });

    app.get('/get-memos', (req, res) => {
        if (req.session.isAuthorized) {
            console.log("Get memos");
            dbf.getAll(dbo, (data => {
                res.json({
                    auth : true,
                    data : data
                });
            }));
        }
        else {
            console.log("Unauthorized");
            res.json({ auth : false });
        }
    });
    
    app.post('/post-memo', (req,res) => {
        //console.log(req.body);
    
        let title = req.body.memo_title;
        let content = req.body.memo_content;

        let insertable = {
            title : title,
            content : content
        }

        dbf.insertOne(dbo, insertable, (db_result => {
            res.json(db_result);
        }));
    });

    app.post('/remove-memo', (req, res) => {
        let id = req.body._id;
        console.log(`Receive remove POST for id ${id}`);

        dbf.removeOne(dbo, id, (db_result) => {
            res.json(db_result);
        });
    });
});


