const { ObjectID } = require("mongodb");

class Database {
    getUsers(dbo, username, pw, cb) {
        dbo.collection("users").find({username : username, password : pw}).toArray((err, result) => {
            if (err) throw err;
            cb(result);
        });
    }
    
    getAll(dbo, cb) {
        dbo.collection("memos").find({}).toArray((err, result) => {
            if (err) throw err;
            cb(result);
        });
    }

    insertOne(dbo, insertable, cb) {
        dbo.collection("memos").insertOne(insertable, (err, result) => {
            if (err) {
                cb({
                    result : "error",
                    msg : err
                });
            }
            else {
                cb({
                    result : "success",
                    msg : null
                });
            }
        });
    }

    removeOne(dbo, id, cb) {
        let query = { _id : new ObjectID(id) };
        console.log(query);
        
        dbo.collection("memos").deleteOne(query, (err, result) => {
            console.log(result.result);
            if (err) {
                cb({
                    result : "error",
                    msg : err
                });
            }
            else {
                cb({
                    result : "success",
                    msg : null
                });
            }
        });
    }
}


module.exports = Database;